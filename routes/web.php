<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('index',[
	'as'=>'index',
	'uses'=>'IndexController@getIndex'
]);

Route::get('/',[
	'as'=>'index',
	'uses'=>'IndexController@getIndex'
]);

Route::get('login',[
	'as'=>'login',
	'uses'=>'LoginController@getLogin'
]);

Route::get('register',[
	'as'=>'register',
	'uses'=>'RegisterController@getRegister'
]);

// for admin route
Route::prefix('admin')->group(function () {
	Route::get('index',[
	'as'=>'index',
	'uses'=>'AdminController@getIndex'
]);
});
