<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" style="color: #e24545;" href="public/user-assets/images/icon.jpg" type="image/x-icon"/>
    <title> Home | Ybox </title>
    <link rel='stylesheet' href='https://cdn.rawgit.com/daneden/animate.css/v3.1.0/animate.min.css'>
    <link rel="stylesheet" href="public/user-assets/css/font-awesome.css">
    <link rel="stylesheet" href="public/user-assets/css/chosen.min.css">
    <link rel="stylesheet" href="public/user-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="public/user-assets/css/index.css">
    <link rel="stylesheet" type="text/css" href="public/user-assets/css/footer.css">
    <link rel="stylesheet" type="text/css" href="public/user-assets/css/rating.css">
</head>
<body>
    <!-- for header + menu -->
    <header class="index">
        <div class="container">
          <nav class="navbar navbar-default sub-menu" id="menu" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand main-nav-brand" href="/Ybox-review/index"><img src="public/user-assets/images/logo.png"></img></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right menu-list">
                <li class="menu-list-items"><a href="/Ybox-review/category">Category</a></li>
                <li class="menu-list-items"><a href="/Ybox-review/login">Login</a></li>
                <li class="menu-list-items"><a href="/Ybox-review/register" >Signup</a></li>
                <li class="menu-list-items"><button type="button" id="forcompany" class="btn">For Company</button></li>
              </ul>
            </div>
          </nav>
          <div class="scene-overlay banner">
              <h1 class="banner-title">Review and discover great companies!</h1>
              <h2 class="banner-description">Read reviews. Write reviews. Shop smarter.</h2>
        <div class="container">
          <div class="row">
            <form class="form-inline banner-search col-md-11" role="form" action="#">
              <div class="form-group has-feedback col-xs-12 col-sm-4 col-md-4">
                <input  class="form-control feedback" placeholder="Name Center" />
                <i class="form-control-feedback feedbacki glyphicon glyphicon-search"></i>
              </div><!--
              --><div class="form-group has-feedback col-xs-12 col-sm-3 col-md-3">
                <select id="subjectList" name="selectbasic" class="form-control my_select_box feedback">
                  <option value="Courses">All Courses</option>
                  <option value="English for kid">English for kid</option>
                  <option value="English for adult">English for adult</option>
                  <option value="TOEIC">TOEIC</option>
                  <option value="IELTS">IELTS</option>
                  <option value="English for Guider">English for Guider</option>
                </select>
                <i class="form-control-feedback feedbacki glyphicon glyphicon-list"></i>
              </div><!--
              --><div class="form-group has-feedback col-xs-12 col-sm-3 col-md-3">
                <select id="locationList" name="selectbasic" class="form-control my_select_box feedback">
                  <option value="Locations">All Locations</option>
                  <option value="Ha Noi">Ha Noi </option>
                  <option value="Ho Chi Minh">Ho Chi Minh</option>
                  <option value="Da Nang">Da Nang</option>
                  <option value="Can Tho">Can Tho</option>
                  <option value="Ha Tinh">Ha Tinh</option>
                  <option value="Quang Tri">Quang Tri</option>
                </select>
                <i class="form-control-feedback feedbacki glyphicon glyphicon-map-marker"></i>
              </div><!--
              --><div class="form-group has-feedback col-xs-12 col-sm-2 col-md-2">
                <button type="submit" class="btn btn-default find">Find Company</button>
              </div><!--
                
              --><div></div>
            </form>
          </div>
        </div>
          </div>
        </div>
      </header>
      <!-- recent review -->
      <div class="container-fluid">
        <div class="row">
          <div class="choose-feed">
            <ul>
            <li>
              Recent Reviews
              <span class="triangle-down"></span>
            </li>
          </ul>
          </div>
        </div>
      </div>
      <!-- for content -->
      <div class="content">
        <div class="container">
          <div class="row recent-reviews">
            <div class="col-sm-6 col-xs-12 col-sm-12 column">
              <div class="review-block">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12 col-sm-12 column">
                      <img src="public/user-assets/images/bg.jpg"  class="img-circle">
                      <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                    </div>
                    <div class="col-sm-9 col-xs-12 col-sm-12 column">
                      <div class="review-block-rate">
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                      </div>
                      <div class="review-block-title "><strong>LinhLinh</strong> đã review cho <strong>KFC</strong></div>
                      <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 col-sm-12 column">
              <div class="review-block">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12 col-sm-12 column">
                      <img src="public/user-assets/images/bg.jpg" class="img-circle">
                      <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                    </div>
                    <div class="col-sm-9 col-xs-12 col-sm-12 column">
                      <div class="review-block-rate">
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                      </div>
                      <div class="review-block-title "><strong>LinhLinh</strong> đã review cho <strong>KFC</strong></div>
                      <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy
                      this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row recent-reviews">
            <div class="col-sm-6 col-xs-12 col-sm-12 column">
              <div class="review-block">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12 col-sm-12 column">
                      <img src="public/user-assets/images/bg.jpg"  class="img-circle">
                    </div>
                    <div class="col-sm-9 col-xs-12 col-sm-12 column">
                      <div class="review-block-rate">
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                      </div>
                      <div class="review-block-title "><strong>LinhLinh</strong> đã review cho <strong>KFC</strong></div>
                      <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 col-sm-12 column">
              <div class="review-block">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12 col-sm-12 column ">
                      <img src="public/user-assets/images/bg.jpg" class="img-circle">
                      <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                    </div>
                    <div class="col-sm-9 col-xs-12 col-sm-12 column">
                      <div class="review-block-rate">
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                      </div>
                      <div class="review-block-title "><strong>LinhLinh</strong> đã review cho <strong>KFC</strong></div>
                      <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row recent-reviews">
            <div class="col-sm-6 col-xs-12 col-sm-12 column">
              <div class="review-block">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12 col-sm-12 column">
                      <img src="public/user-assets/images/bg.jpg"  class="img-circle">
                      <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                    </div>
                    <div class="col-sm-9 col-xs-12 col-sm-12 column">
                      <div class="review-block-rate">
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                      </div>
                      <div class="review-block-title "><strong>Linh</strong> đã review cho <strong>KFC</strong></div>
                      <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy
                      this was nice in buy. this wa was nice in buy this was nice in buy this was nice in buy</div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 col-sm-12 column">
              <div class="review-block">
                  <div class="row">
                    <div class="col-sm-3 col-xs-12 col-sm-12 column">
                      <img src="public/user-assets/images/bg.jpg" class="img-circle">
                      <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                    </div>
                    <div class="col-sm-9 col-xs-12 col-sm-12 column">
                      <div class="review-block-rate">
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                          <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        </button>
                      </div>
                      <div class="review-block-title "><strong>Linh</strong> đã review cho <strong>KFC</strong></div>
                      <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--footer start from here-->
      <footer>
      <div class="container">
          <div class="row">
              <div class="col-xs-12 col-sm-3 col-md-3 column">
                  <div data-toggle="collapse" class="taber" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      ABOUT
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                     <ul class="footer-ul">
                        <li><a href="#"> About</a></li>
                        <li><a href="#"> Jobs</a></li>
                        <li><a href="#"> Contact</a></li>
                        <li><a href="#"> Blog</a></li>
                        <li><a href="#"> Press</a></li>
                      </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 column">
                  <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapseOne">
                      COMMUNITY
                  </div>
                  <div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <ul class="footer-ul">
                      <li><a href="#"> Trust in reviews</a></li>
                      <li><a href="#">Support Center</a></li>
                      <li><a href="#"> Log in</a></li>
                      <li><a href="#"> Sign up</a></li>
                      <li><a href="#"> Chrome App</a></li>
                    </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 column">
                <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapseOne">
                  BUSINESSES
                </div>
                  <div id="collapsethree" class="panel-collapse collapse in menufooter" role="tabpanel" aria-labelledby="headingOne">
                    <ul class="footer-ul">
                      <li><a href="#"> Ybox business</a></li>
                      <li><a href="#"> Products</a></li>
                      <li><a href="#">Plans and Pricing</a></li>
                      <li><a href="#"> Business Login</a></li>
                    </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-3 column">
                <div  class="taber">
                  FOLLOW US
                </div>
                <div class="text-left  center-block">
                          <a href="https://www.Ybox.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                        <a href="https://Ybox.com/Ybox"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                        <a href=""><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                        <a href="Ybox@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                        <a href="Ybox@gmail.com"><i id="social-em" class="fa fa-youtobe-square fa-3x social"></i></a>
                        <a href="Ybox@gmail.com"><i id="social-fb" class="fa fa-linkedin-square fa-3x social"></i></a>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d433868.0837064906!2d35.66744174160663!3d31.836036762053016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151b5fb85d7981af%3A0x631c30c0f8dc65e8!2sAmman!5e0!3m2!1sen!2sjo!4v1499168051085" sytle="" frameborder="0" style="border:0 ; padding-top: 10px;" allowfullscreen></iframe>
              </div>
          </div>
      </div>
      <!-- end of footer -->
      <div class="copyright">
        <div class="container">
        <div class="col-md-6">
          <p>© 2017, Ybox, All rights reserved</p>
        </div>
        </div>
      </div>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="public/user-assets/javascript/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script type="text/javascript" src="public/user-assets/javascript/index.js"></script>
  <script type="text/javascript" src="public/user-assets/javascript/chosen.jquery.min.js"></script>
  <script>
    $(document).ready(function() { 
       $(".my_select_box").chosen();
       $('.chosen-single').css('height', 45);
       $('.chosen-single').css('padding-top', 10);
    });
  </script>
  
</body>
</html>
