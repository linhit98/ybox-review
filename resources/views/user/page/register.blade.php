@extends('user.layout.master')
@section('content')
<div class="wow fadeInLeft" >
  <div class="header-form">
   <h2>Register</h2>
  </div>
  <form method="post" action="register.php">
    <div class="form-group">
      <label>Username</label>
      <input class="form-control" required type="text" name="username" required>
    </div>
    <div class="form-group">
      <label>Email</label>
      <input class="form-control" required type="email" name="email" required>
    </div>
    <div class="form-group">
      <label>Password</label>
      <input class="form-control" required type="password" name="password_1" required>
    </div>
    <div class="form-group">
      <label>Confirm password</label>
      <input class="form-control" required type="password" name="password_2" required>
    </div>
    <div class="form-group">
      <button type="submit" class="btnlog" name="reg_user">Register</button>
    </div>
    <p>
      Already a member? <a href="login.html">Sign in</a>
    </p>
    <div class="omb_login">
    <div class="row  omb_socialButtons">
        <div class="col-xs-4 col-sm-6">
          <a href="#" class="btn btn-lg btn-block omb_btn-facebook">
            <i class="fa fa-facebook visible-xs"></i>
            <span class="hidden-xs">Facebook</span>
          </a>
        </div>
        <div class="col-xs-4 col-sm-6">
          <a href="#" class="btn btn-lg btn-block omb_btn-google">
            <i class="fa fa-google-plus visible-xs"></i>
            <span class="hidden-xs">Google+</span>
          </a>
        </div>
      </div>
    </div>
  </form> 
</div>
@endsection