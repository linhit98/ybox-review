@extends('user.layout.master')
@section('content')
<div class="wow fadeInLeft " >
    <div class="header-form">
       <h2>Login</h2>
    </div>
    <form method="post" action="login.php" >
      <div class="form-group">
        <label>Username</label>
        <input  class="form-control" required type="text" name="username" >
      </div>
      <div class="form-group">
        <label>Password</label>
        <input class="form-control" type="password" required name="password">
      </div>
      <div class="form-group">
        <button type="submit" class="btnlog" name="login_user">Login</button>
      </div>
      <p>
        Not yet a member? <a href="signup.html">Sign up</a>
      </p>
      <div class="omb_login">
	      <div class="row  omb_socialButtons">
	          <div class="col-xs-4 col-sm-6">
	            <a href="#" class="btn btn-lg btn-block omb_btn-facebook">
	              <i class="fa fa-facebook visible-xs"></i>
	              <span class="hidden-xs">Facebook</span>
	            </a>
	          </div>
	          <div class="col-xs-4 col-sm-6">
	            <a href="#" class="btn btn-lg btn-block omb_btn-google">
	              <i class="fa fa-google-plus visible-xs"></i>
	              <span class="hidden-xs">Google+</span>
	            </a>
	          </div>
	       </div>
      </div>
    </form>
</div>
@endsection