<!-- for header + menu -->
    <header class="login-header">
      <div class="container">
        <nav class="navbar navbar-default sub-menu" id="menu" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/Ybox-review/index"><img src="public/user-assets/images/logo.png"></a>
          </div>
          <div class="collapse navbar-collapse login-menu" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right menu-list">
              <li class="menu-list-items"><a href="/Ybox-review/category">Category</a></li>
              <li class="menu-list-items"><a href="/Ybox-review/login">Login</a></li>
              <li class="menu-list-items"><a href="/Ybox-review/register" >Signup</a></li>
              <li class="menu-list-items"><button type="button" id="forcompany" class="btn">For Company</button></li>
            </ul>
          </div>
        </nav>
      </div>
    </header>