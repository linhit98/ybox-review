<!DOCTYPE html>
<html lang="en">
  <head>
	    <meta charset="utf8mb4">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link rel="shortcut icon" style="color: #e24545;" href="public/user-assets/images/logo.jpg" type="image/x-icon"/>
	    <title>user | Homepage</title>
	    <meta name="csrf-token" content="{{ csrf_token() }}">
		@include('user.layout.cssfile')
	</head>
	<body>
		@include('user.layout.header')
		<div class="container-fluid">
		@yield('content')
		</div>
		@include('user.layout.footer')
		@include('user.layout.jsfile')
	</body>
</html>
