  <!-- footer start from here -->
 <footer>
      <div class="container">
          <div class="row">
              <div class="col-xs-12 col-sm-3 col-md-3 column">
                  <div data-toggle="collapse" class="taber" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      ABOUT
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                     <ul class="footer-ul">
                        <li><a href="#"> About</a></li>
                        <li><a href="#"> Jobs</a></li>
                        <li><a href="#"> Contact</a></li>
                        <li><a href="#"> Blog</a></li>
                        <li><a href="#"> Press</a></li>
                      </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 column">
                  <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapseOne">
                      COMMUNITY
                  </div>
                  <div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <ul class="footer-ul">
                      <li><a href="#"> Trust in reviews</a></li>
                      <li><a href="#">Support Center</a></li>
                      <li><a href="#"> Log in</a></li>
                      <li><a href="#"> Sign up</a></li>
                      <li><a href="#"> Chrome App</a></li>
                    </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 column">
                <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapseOne">
                  BUSINESSES
                </div>
                  <div id="collapsethree" class="panel-collapse collapse in menufooter" role="tabpanel" aria-labelledby="headingOne">
                    <ul class="footer-ul">
                      <li><a href="#"> Ybox business</a></li>
                      <li><a href="#"> Products</a></li>
                      <li><a href="#">Plans and Pricing</a></li>
                      <li><a href="#"> Business Login</a></li>
                    </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-3 column">
                <div  class="taber">
                  FOLLOW US
                </div>
                <div class="text-left  center-block">
                          <a href="https://www.ybox.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                        <a href="https://ybox.com/ybox"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                        <a href=""><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                        <a href="ybox@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                        <a href="ybox@gmail.com"><i id="social-em" class="fa fa-youtobe-square fa-3x social"></i></a>
                        <a href="ybox@gmail.com"><i id="social-fb" class="fa fa-linkedin-square fa-3x social"></i></a>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d433868.0837064906!2d35.66744174160663!3d31.836036762053016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151b5fb85d7981af%3A0x631c30c0f8dc65e8!2sAmman!5e0!3m2!1sen!2sjo!4v1499168051085" sytle="" frameborder="0" style="border:0 ; padding-top: 10px;" allowfullscreen></iframe>
              </div>
          </div>
      </div>
      <!-- end of footer -->
      <div class="copyright">
        <div class="container">
        <div class="col-md-6">
          <p>© 2017, Ybox, All rights reserved</p>
        </div>
        </div>
      </div>
  </footer>