<base href="{{asset('')}}">

<link href="public/admin-assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="public/admin-assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="public/admin-assets/css/zabuto_calendar.css">
<link rel="stylesheet" type="text/css" href="public/admin-assets/js/gritter/css/jquery.gritter.css" />
<link rel="stylesheet" type="text/css" href="public/admin-assets/lineicons/style.css">    

<!-- Custom styles for this template -->
<link href="public/admin-assets/css/style.css" rel="stylesheet">
<link href="public/admin-assets/css/style-responsive.css" rel="stylesheet">

<script src="public/admin-assets/js/chart-master/Chart.js"></script>