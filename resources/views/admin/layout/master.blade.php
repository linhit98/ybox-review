<!DOCTYPE html>
<html lang="en">
  <head>
	    <meta charset="utf8mb4">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link rel="shortcut icon" style="color: #e24545;" href="public/user-assets/images/admin.png" type="image/x-icon"/>
	    <title>Admin | Homepage</title>
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <meta name="description" content="">
		@include('admin.layout.cssfile')
	</head>
	<body>
		<section id="container" >
		@include('admin.layout.header')
		@include('admin.layout.nav')
		<section id="main-content">
		<section class="wrapper">
		@yield('content')
		</section>
		</section>
		@include('admin.layout.footer')
		</section>
		@include('admin.layout.jsfile')
	</body>
</html>
